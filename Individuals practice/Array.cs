using System;

using System.Collections;

namespace Tutlane

{

    class Program

    {

        static void Main(string[] args)

        {

            ArrayList arrlist = new ArrayList();

            arrlist.Add("Welcome");

            arrlist.Add(100);

            arrlist.Add(20);

            arrlist.Add("Rijvi");

            Console.WriteLine("ArrayList Count: " + arrlist.Count);

            Console.WriteLine("ArrayList Capacity: " + arrlist.Capacity);


            ArrayList arrlist2 = new ArrayList() { 10, "Hi" };

            // adding arrlist2 to arrlist

            arrlist.AddRange(arrlist2);
            foreach (var item in arrlist)

            {

                Console.WriteLine(item);

            }
            // accessing element

            string msg = (string)arrlist[0];
            Console.WriteLine("Element at 0: " + msg);
            //Insert array
            // inserting elements into arraylist

            arrlist.Insert(0, "Akash");

            arrlist.Insert(1, 50); 
            ArrayList arrlist3 = new ArrayList() { 200, 300 };
            arrlist.InsertRange(2, arrlist3);
            foreach (var item in arrlist)

            {

                Console.WriteLine(item);

            }
            // Removing an element which is having a value 50

            arrlist.Remove(50);

            // Removing an element at index 0

            arrlist.RemoveAt(0);

            // Removing 2 elements starting from index 3

            arrlist.RemoveRange(3, 2);
            foreach (var item in arrlist)

            {

                Console.WriteLine(item);

            }

            Console.ReadLine();

        }

    }

}