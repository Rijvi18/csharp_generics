using System;

using System.Collections.Generic;

 

namespace DictionaryProblem

{

    class Program

    {

        static void Main(string[] args)

        {

            Dictionary<int, string> dct = new Dictionary<int, string>();

            dct.Add(1, "Rijvi");

            dct.Add(4, "Ahmed");

            dct.Add(2, "Akash");

            dct.Add(3, null);

            // Another way to add elements.

            // If key not exist, then that key adds a new key/value pair.

            dct[5] = "Trishi";

            try

            {

                dct.Add(2, "jony");

            }

            catch (ArgumentException)

            {

                Console.WriteLine("An element with Key = '2' already exists.");

            }

            Console.WriteLine("Dictionary1 Elements:");

            // Accessing elements as KeyValuePair objects.

            foreach (KeyValuePair<int, string> item in dct)

            {

                Console.WriteLine("Key = {0}, Value = {1}", item.Key, item.Value);

            }

 

            // Creating and initializing dictionary

            Dictionary<string, int?> dct2 = new Dictionary<string, int?> {

                                                {"msg2", 1},

                                                {"msg3", 20},

                                                {"msg4", 100},

                                                {"msg1", null}

                                            };

            Console.WriteLine("Dictionary2 Elements:");

            // Accessing elements as KeyValuePair objects.

            foreach (KeyValuePair<string, int?> item in dct2)

            {

                Console.WriteLine("Key = {0}, Value = {1}", item.Key, item.Value);

            }
            //remove elements with key
            dct.Remove(3);
            dct.Remove(5);
            Console.WriteLine("Access Dictionary Elements:");

            Console.WriteLine();

            foreach (KeyValuePair<int, string> item in dct)

            {

                Console.WriteLine("Key = {0}, Value = {1}", item.Key, item.Value);

            }
            Console.ReadLine();

        }

    }

}