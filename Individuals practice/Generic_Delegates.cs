using System;

 

namespace DelegateProblem

{

    // Declare Generic Delegate

    public delegate T SampleDelegate<T>(T a, T b);

    class MathOperations

    {

        public int Add(int a, int b)

        {

            return a + b;

        }

        public int Subtract(int x, int y)

        {

            return x - y;

        }

    }

    class Program

    {

        static void Main(string[] args)

        {

            Console.WriteLine("****Generic Delegate Example****");

            MathOperations m = new MathOperations();

            //add method

            SampleDelegate<int> dlgt = new SampleDelegate<int>(m.Add);

            Console.WriteLine("Addition Result: " + dlgt(10, 90));

            //subtract method

            dlgt = m.Subtract;

            Console.WriteLine("Subtraction Result: " + dlgt(10, 90));

            Console.ReadLine();

        }

    }

}