using System;

namespace HashtableProblem

{

    public class GenericClass<T>

    {

        public T msg;

        public void genericMethod(T name, T location)

        {

            Console.WriteLine("{0}", msg);

            Console.WriteLine("Name: {0}", name);

            Console.WriteLine("Location: {0}", location);

        }

    }

    class Program

    {

        static void Main(string[] args)

        {

            Console.WriteLine("****Generics Example****");

            // Instantiate Generic Class, string is the type argument

            GenericClass<string> gclass = new GenericClass<string>();

            gclass.msg = "Welcome to Bangladesh";

            gclass.genericMethod("Rijvi Ahmed", "Dhaka");

            Console.ReadLine();

        }

    }

}