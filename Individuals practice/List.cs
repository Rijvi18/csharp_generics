using System;

using System.Collections.Generic;

 

namespace ListProblem

{

    class Program

    {

        static void Main(string[] args)

        {

            // Creating and initializing list

            List<int> lst = new List<int>();

            lst.Add(1);

            lst.Add(8);

            lst.Add(45);

            List<string> lst2 = new List<string>();

            lst2.Add("Rijvi");

            lst2.Add("Ahmed");

            lst2.Add("Akash");

            Console.WriteLine("List1 Elements Count: " + lst.Count);

            Console.WriteLine("List1 Capacity: " + lst.Capacity);

            // Accessing list elements
            Console.WriteLine("List1 Elements:");
            foreach (var item in lst)

            {

                Console.WriteLine(item);

            }

            Console.WriteLine("List2 Elements Count: " + lst2.Count);

            Console.WriteLine("List2 Capacity: " + lst2.Capacity);

            Console.WriteLine("List2 Elements:");

            foreach (var item in lst2)

            {

                Console.WriteLine(item);

            }
            //inserting elements into list
            List<int> lst3 = new List<int>() { 1, 8, 45, 70 };
            lst3.Insert(0, 10);
            lst3.Insert(3, 50);

            List<int> lst4 = new List<int>() { 200, 300 };

            // inserting lst2 into lst at position 2

            lst3.InsertRange(2, lst4);

            Console.WriteLine("ArrayList Elements:");

            foreach (var item in lst)

            {

                Console.WriteLine(item);

            }
            
            // Removing an element which is having a value 50

            lst.Remove(1);

            // Removing an element at index 2

            //lst.RemoveAt(2);

            // Removing 2 elements starting from index 3

            lst3.RemoveRange(1, 2);

            // Check for an item a exists in list or not
            Console.WriteLine("Item Exists: " + lst4.Contains(70));

            Console.ReadLine();

        }

    }

}