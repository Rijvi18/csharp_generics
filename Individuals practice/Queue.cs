using System;

using System.Collections;

 

namespace QueueProblem

{

    class Program

    {

        static void Main(string[] args)

        {

            // Create and initialize a queue

            Queue que = new Queue();

            que.Enqueue("Rijvi");

            que.Enqueue("Ahmed");

            que.Enqueue(20);

            que.Enqueue(1);

            que.Enqueue(10);

            Console.WriteLine("Number of Elements in Queue: {0}", que.Count);

           // Access Queue Elements 

            foreach (var item in que)

            {

                Console.WriteLine(item);

            }
            //Contain Elements
            Console.WriteLine("Contains Element 100: {0}", que.Contains(10));

            Console.WriteLine("Contains Key 'Hello': {0}", que.Contains("Hello"));

            //peek elements
            Console.WriteLine(que.Peek());

            Console.WriteLine(que.Peek());

            //Dequeue elements
            while (que.Count > 0)

            {

                Console.WriteLine(que.Dequeue());

            }

            Console.WriteLine("Number of Elements in Queue: {0}", que.Count);

            Console.ReadLine();

        }

    }

}