using System;

using System.Collections.Generic;

 

namespace SortedList

{

    class Program

    {

        static void Main(string[] args)

        {

            SortedList<int, string> slst = new SortedList<int, string>();

            slst.Add(1, "Rijvi");

            slst.Add(4, "Ahmed");

            slst.Add(2, "Akash");

            slst.Add(3, null);

            slst[5] = "Riya";

            // Add method throws exception if key already in the list

            try

            {

                slst.Add(2, "Towfiq");

            }

            catch (ArgumentException)

            {

                Console.WriteLine("An element with Key = '2' already exists.");

            }        

            Console.WriteLine("List1 elements:");

            // Accessing elements as KeyValuePair objects.

            foreach (KeyValuePair<int, string> item in slst)

            {

                Console.WriteLine("Key = {0}, Value = {1}", item.Key, item.Value);

            }

 

            // Creating and initializing list

            SortedList<string, int?> slst2 = new SortedList<string, int?> {

                                                {"msg2", 1},

                                                {"msg3", 20},

                                                {"msg4", 100},

                                                {"msg1", null}

                                            };

            Console.WriteLine("List2 elements:");

            // Accessing elements as KeyValuePair objects.

            foreach (KeyValuePair<string, int?> item in slst2)

            {

                Console.WriteLine("Key = {0}, Value = {1}", item.Key, item.Value);

            }

            Console.ReadLine();

        }

    }

}