using System;

using System.Collections;

 

namespace Tutlane

{

    class Program

    {

        static void Main(string[] args)

        {

            // Create and initialize a stack

            Stack stk = new Stack();

            stk.Push("Rijvi");

            stk.Push("Ahmed");

            stk.Push(20);

            stk.Push(10);

            stk.Push(null);

            stk.Push(100);

            Console.WriteLine("Number of Elements in Stack: {0}", stk.Count);

            // Access Stack Elements 

            foreach (var item in stk)

            {

                Console.WriteLine(item);

            }
            while (stk.Count > 0)

            {

                Console.WriteLine(stk.Pop());

            }

            Console.WriteLine("Number of Elements in Stack: {0}", stk.Count);

            Console.ReadLine();

        }

    }

}